{-# language OverloadedStrings #-}

module Run where

import SDL qualified
import SDL.Vect (V2(..))
import Game qualified
import Events
import Model
import Render
import Update

run :: IO ()
run =
  Game.withWindow windowConfig $ \window ->
    withGL window $ Game.loop (Game.RenderFPS 60) (Game.UpdateFPS 300) (game window)

windowConfig :: Game.WindowConfig
windowConfig = Game.WindowConfig
  { Game.title = "SDL2 OpenGL"
  , Game.windowConfig = SDL.defaultWindow
    { SDL.windowInitialSize = V2 screenWidth screenHeight
    , SDL.windowGraphicsContext =
      SDL.OpenGLContext SDL.defaultOpenGL
        { SDL.glProfile = SDL.Core SDL.Debug 4 6
        }
    }
  }

game :: SDL.Window -> Game.Game GameState MyEvents
game window = Game.Game
  { Game.state = initial
  , Game.update = update
  , Game.render = render window
  , Game.fetchEvents = fetchEvents
  , Game.isQuit = const eQuit
  }
