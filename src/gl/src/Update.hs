
module Update where

import Model
import Events

update :: MyEvents -> GameState -> IO GameState
update _ state = pure state
