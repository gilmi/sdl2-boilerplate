module Events where

import SDL qualified

data MyEvents = MyEvents
  { eQuit :: Bool
  , eArrowUp :: Bool
  , eArrowDown :: Bool
  , eArrowLeft :: Bool
  , eArrowRight :: Bool
  }
  deriving Show

fetchEvents :: IO MyEvents
fetchEvents = do
  events <- SDL.pollEvents
  isKeyPressed <- SDL.getKeyboardState
  pure $ MyEvents
    { eQuit = elem SDL.QuitEvent $ map SDL.eventPayload events
    , eArrowUp = isKeyPressed SDL.ScancodeUp
    , eArrowDown = isKeyPressed SDL.ScancodeDown
    , eArrowLeft = isKeyPressed SDL.ScancodeLeft
    , eArrowRight = isKeyPressed SDL.ScancodeRight
    }
