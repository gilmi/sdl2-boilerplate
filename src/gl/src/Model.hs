module Model where

import Foreign.C.Types (CInt)

data GameState
  = GameState

initial :: GameState
initial = GameState

screenWidth, screenHeight :: CInt
(screenWidth, screenHeight) = (720, 576)
