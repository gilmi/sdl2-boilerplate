{-# language QuasiQuotes #-}

module Render where

import Data.Word
import Data.Int
import SDL qualified
import Graphics.GL.Core41 qualified as GL
import Graphics.GL.Types qualified as GL
import Foreign qualified as F (alloca, peek)
import Foreign.Ptr qualified as F (Ptr, castPtr, nullPtr, plusPtr)
import Foreign.C.String qualified as F (withCString)
import Foreign.Storable qualified as F (sizeOf)
import Foreign.C.Types qualified as F
import Foreign.Marshal.Utils qualified as F (with)
import Foreign.Marshal.Array qualified as F (withArray)
import Text.RawString.QQ (r)

render :: SDL.Window -> a -> IO ()
render window _ = do
  GL.glClearColor 0 0 0 1
  GL.glClear GL.GL_COLOR_BUFFER_BIT
  GL.glDrawArrays GL.GL_TRIANGLES 0 3
  SDL.glSwapWindow window

withGL :: SDL.Window -> IO () -> IO ()
withGL window go = do
  -- Create GL context
  context <- SDL.glCreateContext window
  SDL.glMakeCurrent window context
  -- Create Vertex Array Object
  vao <- F.alloca $ \vao -> GL.glGenVertexArrays 1 vao >> F.peek vao
  GL.glBindVertexArray vao
  -- Create a Vertex Buffer Object and copy the vertex data to it
  vbo <- F.alloca useVBO
  -- Create and compile the vertex shader
  Just posShader <- compileShader GL.GL_VERTEX_SHADER positionShader
  -- Create and compile the fragment shader
  Just clrShader <- compileShader GL.GL_FRAGMENT_SHADER colorShader
  -- Link the vertex and fragment shader into a shader program
  shaderProgram <- createShaderProgram [(posShader, Nothing), (clrShader, Just (0, "outcolor"))]
  -- Specify the layout of the vertex data
  posAttrib <- F.withCString "position" $ GL.glGetAttribLocation shaderProgram
  GL.glEnableVertexAttribArray (fromIntegral posAttrib)
  GL.glVertexAttribPointer (fromIntegral posAttrib) 2 GL.GL_FLOAT  GL.GL_FALSE 20 (F.plusPtr F.nullPtr 0)
  --
  clrAttrib <- F.withCString "color" $ GL.glGetAttribLocation shaderProgram
  GL.glEnableVertexAttribArray (fromIntegral clrAttrib)
  GL.glVertexAttribPointer (fromIntegral clrAttrib) 3 GL.GL_FLOAT  GL.GL_FALSE 20 (F.plusPtr F.nullPtr (F.sizeOf (undefined :: GL.GLfloat) * 2))
  --

  go

  -- Cleanup
  GL.glDeleteProgram shaderProgram
  GL.glDeleteShader posShader
  GL.glDeleteShader clrShader
  F.with vbo $ GL.glDeleteBuffers 1
  F.with vao $ GL.glDeleteVertexArrays 1

  SDL.glDeleteContext context

-- Create a Vertex Buffer Object and copy the vertex data to it
useVBO :: F.Ptr GL.GLuint -> IO Word32
useVBO vboPtr = do
  vbo <- GL.glGenBuffers 1 vboPtr >> F.peek vboPtr
  GL.glBindBuffer GL.GL_ARRAY_BUFFER vbo
  F.withArray (map F.CFloat [0, 0.5, 1, 0, 0, 0.5, -0.5, 0, 1, 0, -0.5, -0.5, 0, 0, 1]) (sendVerticesToGPU (4*3*5))
  pure vbo

-- copy vertex data to gpu
sendVerticesToGPU :: Int64 -> F.Ptr F.CFloat -> IO ()
sendVerticesToGPU size vertices_array =
  GL.glBufferData GL.GL_ARRAY_BUFFER (F.CPtrdiff size) (F.castPtr vertices_array) GL.GL_STATIC_DRAW

-- Create Shader program, attach shaders, link and use
createShaderProgram :: [(GL.GLuint, Maybe (GL.GLuint, String))] -> IO GL.GLuint
createShaderProgram shaders = do
  shaderProgram <- GL.glCreateProgram
  mapM_ (GL.glAttachShader shaderProgram . fst) shaders
  mapM_ (maybe (pure ()) (uncurry (bindFrag shaderProgram)) . snd) shaders
  GL.glLinkProgram shaderProgram
  GL.glUseProgram shaderProgram
  pure shaderProgram

bindFrag :: GL.GLuint -> GL.GLuint -> String -> IO ()
bindFrag shaderProgram clr name = F.withCString name (GL.glBindFragDataLocation shaderProgram clr)

-- compile shader
compileShader :: GL.GLenum -> String -> IO (Maybe GL.GLuint)
compileShader shaderType shaderSource = do
  shader <- GL.glCreateShader shaderType
  F.withCString shaderSource $ flip F.with (\shaderSourcePtr -> GL.glShaderSource shader 1 shaderSourcePtr F.nullPtr)
  GL.glCompileShader shader
  success <- F.alloca $ \status -> GL.glGetShaderiv shader GL.GL_COMPILE_STATUS status >> F.peek status
  print $ success == GL.GL_TRUE
  if success == GL.GL_TRUE then (pure . Just) shader else pure Nothing

-- Shaders

type Shader = String

positionShader :: Shader
positionShader =
  [r|
#version 410 core

in vec2 position;
in vec3 color;

out vec3 Color;

void main()
{
  Color = color;
  gl_Position = vec4(position, -0.5, 1.0);
}
|]

colorShader :: Shader
colorShader =
  [r|
#version 410 core

in vec3 Color;

out vec4 outColor;

void main()
{
  //outColor = vec4(1.0, 1.0, 1.0, 1.0);
  outColor = vec4(Color, 1.0);
}
|]
