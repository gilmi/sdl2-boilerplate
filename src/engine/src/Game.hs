module Game
  ( withWindow
  , withRenderer
  , loop
  , Game(..)
  , WindowConfig(..)
  , RenderFPS(..)
  , UpdateFPS(..)
  )
  where

import Data.Text (Text)
import Data.Word (Word32)
import Control.Monad (when, unless)
import Control.Concurrent (threadDelay)
import SDL.Vect (V4(..))
import SDL (($=))
import System.IO (hPutStrLn, stderr)
import Data.IORef (newIORef, atomicWriteIORef, readIORef)
import Control.Concurrent.Async (race_)
import qualified SDL
import GHC.Stack (HasCallStack)

-- * Initialize

-- | Create a window
withWindow :: HasCallStack => WindowConfig -> (SDL.Window -> IO ()) -> IO ()
withWindow winCfg play = do
  SDL.initializeAll
  window <- SDL.createWindow (title winCfg) (windowConfig winCfg)
  SDL.showWindow window
  play window
  SDL.destroyWindow window
  SDL.quit

-- | Create a renderer
withRenderer :: HasCallStack => SDL.Window -> (SDL.Renderer -> IO ()) -> IO ()
withRenderer window play = do
  SDL.HintRenderScaleQuality $= SDL.ScaleLinear
  renderQuality <- SDL.get SDL.HintRenderScaleQuality
  when (renderQuality /= SDL.ScaleLinear) $
    hPutStrLn stderr "Warning: Linear texture filtering not enabled!"
  renderer <- SDL.createRenderer window (-1)
    SDL.RendererConfig
       { SDL.rendererType = SDL.AcceleratedRenderer
       , SDL.rendererTargetTexture = False
       }
  SDL.rendererDrawColor renderer $= V4 0 0 0 0 -- black background
  play renderer
  SDL.destroyRenderer renderer

-- * Game Loop

-- | Loop until 'isQuit'
loop :: HasCallStack => RenderFPS -> UpdateFPS -> Game game events -> IO ()
loop (RenderFPS renderFPS) (UpdateFPS updateFPS) game = do
  stateRef <- newIORef (state game)
  let
    updateLoop = do
      currentState <- readIORef stateRef
      next' <-
        withFPS updateFPS $ do
          events <- fetchEvents game
          state' <- update game events currentState
          atomicWriteIORef stateRef state'
          pure $
            unless
              (isQuit game state' events)
              updateLoop
      next'
    renderLoop = do
      currentState <- readIORef stateRef
      action <- withFPS renderFPS $ do
        render game currentState
        pure renderLoop
      action
  race_ updateLoop renderLoop

-- | Run in one FPS frame
withFPS :: Word32 -> IO (IO a) -> IO (IO a)
withFPS fps action = do
  begin <- SDL.ticks
  result <- action
  end <- SDL.ticks
  regulateFPS fps begin end
  pure result

regulateFPS :: Word32 -> Word32 -> Word32 -> IO ()
regulateFPS fps begin end
  | fps == 0 = pure ()
  | otherwise = do
    let ticksPerFrame = 1000 `div` fps
        interval = end - begin
        gap = ticksPerFrame - interval
        delayFor
          | gap < ticksPerFrame = fromIntegral $ max 0 gap
          | otherwise = fromIntegral ticksPerFrame
    threadDelay $ delayFor * 1000 -- threadDelay works in microseconds

-- * Types

data Game state events
  = Game
    { state :: state
    , update :: events -> state -> IO state
    , render :: state -> IO ()
    , fetchEvents :: IO events
    , isQuit :: state -> events -> Bool
    }

data WindowConfig
  = WindowConfig
    { title :: Text
    , windowConfig :: SDL.WindowConfig
    }

newtype RenderFPS = RenderFPS Word32
newtype UpdateFPS = UpdateFPS Word32
