{-# language OverloadedStrings #-}

module Run where

import SDL qualified
import SDL.Vect (V2(..))
import Game qualified
import Events
import Model
import Render
import Update

run :: IO ()
run =
  Game.withWindow windowConfig $ \window ->
    Game.withRenderer window $ \renderer ->
      Game.loop (Game.RenderFPS 60) (Game.UpdateFPS 300) (game window renderer)

windowConfig :: Game.WindowConfig
windowConfig = Game.WindowConfig
  { Game.title = "Tracing"
  , Game.windowConfig = SDL.defaultWindow
    { SDL.windowInitialSize = V2 screenWidth screenHeight
    }
  }

game :: SDL.Window -> SDL.Renderer -> Game.Game GameState MyEvents
game _window renderer = Game.Game
  { Game.state = initial
  , Game.update = update
  , Game.render = render renderer
  , Game.fetchEvents = fetchEvents
  , Game.isQuit = const eQuit
  }
