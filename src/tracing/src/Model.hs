module Model where

import Prelude hiding (head, init, tail)
import Data.Word (Word8)
import Foreign.C.Types (CInt)
import SDL.Vect (V2(..), V3(..))

data GameState
  = GameState
    { actions :: [PutPixel]
    , point :: V2 CInt
    }

data PutPixel
  = PutPixel
    { location :: V2 CInt
    , rgb :: V3 Word8
    }

initial :: GameState
initial = GameState
  { actions = []
  , point = V2 100 100
  }

getX :: V2 CInt -> CInt
getX (V2 locX _) = locX

getY :: V2 CInt -> CInt
getY (V2 _ locY) = locY

blockSize :: CInt
blockSize = 24

screenWidth, screenHeight :: CInt
(screenWidth, screenHeight) = (720, 576)
