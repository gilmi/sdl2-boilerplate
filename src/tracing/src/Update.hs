
module Update where

import Data.Bool
import SDL (V2(..), V3(..))
import Model
import Events

update :: MyEvents -> GameState -> IO GameState
update events state = do
  let
    p =
      V2
        ( sum
          [ getX (point state)
          , bool 0 (-1) (eArrowLeft events)
          , bool 0 ( 1) (eArrowRight events)
          ]
        )
        ( sum
          [ getY (point state)
          , bool 0 (-1) (eArrowUp events)
          , bool 0 ( 1) (eArrowDown events)
          ]
        )
  pure $
    state
      { actions =
        [ PutPixel (V2 (getX p + x) (getY p + y)) (V3 100 100 100)
        | x <- [ 100 .. 200 ]
        , y <- [ 50 .. 60 ]
        ]
      , point = p
      }
