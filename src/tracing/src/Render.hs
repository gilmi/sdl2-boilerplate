module Render where

import Data.Foldable (for_)
import Prelude hiding (head, init, tail)
import SDL.Vect (Point(P), V3(..), V4(..))
import SDL (($=))
import SDL qualified
import Model

render :: SDL.Renderer -> GameState -> IO ()
render renderer state = do
  SDL.rendererDrawColor renderer $= V4 0 0 0 0
  SDL.clear renderer
  for_ (actions state) $ \(PutPixel location (V3 r g b)) -> do
    SDL.rendererDrawColor renderer $= V4 r g b 0
    -- SDL.fillRect renderer $ Just $ SDL.Rectangle (P location) snakeBodyBlockSize
    SDL.drawPoint renderer (P location)

  SDL.present renderer
